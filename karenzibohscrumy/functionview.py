def home(request):
    users = User.objects.all()

    def get_by_status(status_name):
        goals = GoalStatus.objects.get(status_name=status_name)
        status_goals = goals.scrumygoals_set.all()
        return status_goals

    daily_goals = get_by_status("Daily Goal")
    weekly_goals = get_by_status("Weekly Goal")
    verify_goals = get_by_status("Verify Goal")
    done_goals = get_by_status("Done Goal")

    dictionary = {
        'users': users,
        'weekly_goals': weekly_goals,
        'daily_goals': daily_goals,
        'verify_goals': verify_goals,
        'done_goals': done_goals,
    }
    return render(request, 'karenzibohscrumy/home.html', dictionary)

    def success_page(request):
    return render(request, 'karenzibohscrumy/successpage.html')


def sign_up(request):
    form = SignUpForm()
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        data = request.POST.dict()
        form.save()
        username = data['username']
        new_user = User.objects.get(username=username)
        developer = Group.objects.get(name='Developer')
        developer.user_set.add(new_user)
        return HttpResponseRedirect('/karenzibohscrumy/successpage')
    else:
        form
    return render(request, 'registration/signup.html', {'form': form})


# @login_required
# def home(request):
#   current_user = {
#     'current_user': request.user,
#     'group': request.user.groups.all()[0].name
#   }
  
#   users = []
#   for user in User.objects.all():
#     weekly_goals = user.goals.filter(goal_status=GoalStatus.objects.get(status_name='Weekly Goal'))
#     daily_goals = user.goals.filter(goal_status=GoalStatus.objects.get(status_name='Daily Goal'))
#     verify_goals = user.goals.filter(goal_status=GoalStatus.objects.get(status_name='Verify Goal'))
#     done_goals = user.goals.filter(goal_status=GoalStatus.objects.get(status_name='Done Goal'))
#     users.append({
#       'username': user.username,
#       'weekly_goals': "<br />".join([f"<a href='movegoal/{goal.id}'>{goal.goal_id} - {goal.goal_name}</a>" for goal in weekly_goals]),
#       'daily_goals': "<br />".join([f"<a href='movegoal/{goal.id}'>{goal.goal_id} - {goal.goal_name}</a>" for goal in daily_goals]),
#       'verify_goals': "<br />".join([f"<a href='movegoal/{goal.id}'>{goal.goal_id} - {goal.goal_name}</a>" for goal in verify_goals]),
#       'done_goals': "<br />".join([f"<a href='movegoal/{goal.id}'>{goal.goal_id} - {goal.goal_name}</a>" for goal in done_goals]),
#     })

#   return render(request, 'almalikmahmudscrumy/home.html', {'users': users, 'user': current_user })




    # form = CreateGoalForm
    # if request.method == 'POST':
    #     random_list = list(random.sample(range(1000, 9999), 10))
    #     random.shuffle(random_list)
    #     random_no = random_list[0]
    #     status = GoalStatus.objects.get(status_name="Daily Goal")

    #     form = form(request.POST)
    #     data = request.POST.dict()
    #     user = User.objects.get(id=data['user'])
    #     add_goal = form.save(commit=False)
    #     add_goal.goal_id = random_no
    #     add_goal.goal_status = status
    #     add_goal.moved_by = user.username
    #     add_goal.created_by = user.username
    #     add_goal.owner = user.username
    #     add_goal.save()
    #     return HttpResponseRedirect('/karenzibohscrumy/home')
    # context = {
    #     'create_goal': form
    # }

    # return render(request, 'karenzibohscrumy/addgoal.html', context)


    form = MoveGoalForm()

    # def get_group(user):
    #     # groups = Group.objects.all()
    #     # is_member = group.user_set.filter(username = user).exists()
    #     user.groups.objects.get(name=group).exists()
    #       # devs = Group.objects.get(name='Developer')
    # print(get_group("jake"))

    # owner = Group.objects.all()

    # print(owner.user_set.get(username="jake"))

    # # qa = Group.objects.get(name='Quality Assurance')
    # # print(qa.user_set.all())

    # # admin = Group.objects.get(name='Admin')
    # # print(admin.user_set.all())
    # # print(get_group('jake'))

    # dictionary = {"error": "A record with that goal id does not exist"}
    # try:
    #     if request.method == 'POST':
    #         data = request.POST.dict()
    #         print(request.user)
    #         goal = ScrumyGoals.objects.get(goal_id=goal_id)
    #         new_status = GoalStatus.objects.get(id=data['goal_status'])
    #         goal.goal_status = new_status
    #         goal.save()
    #         print(goal.goal_status)
    # except ScrumyGoals.DoesNotExist:
    #     return render(request, 'karenzibohscrumy/exception.html',  dictionary)
    # else:
    #     return render(request, 'karenzibohscrumy/movegoal.html', {'form': form})