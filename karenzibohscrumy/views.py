import random
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.contrib.auth.models import Group
from django.contrib.auth.forms import authenticate
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
# from django.utila.decorators import method_decorator


from .models import *
# Create your views here.


@login_required(login_url="/karenzibohscrumy/accounts/login")
def home(request):
    users = User.objects.all()

    def get_by_status(status_name):
        goals = GoalStatus.objects.get(status_name=status_name)
        status_goals = goals.scrumygoals_set.all()
        return status_goals

    daily_goals = get_by_status("Daily Goal")
    weekly_goals = get_by_status("Weekly Goal")
    verify_goals = get_by_status("Verify Goal")
    done_goals = get_by_status("Done Goal")

    dictionary = {
        'users': users,
        'weekly_goals': weekly_goals,
        'daily_goals': daily_goals,
        'verify_goals': verify_goals,
        'done_goals': done_goals,
        'user': request.user,
        'role': request.user.groups.all()[0].name,
    }
    return render(request, 'karenzibohscrumy/home.html', dictionary)


def sign_up(request):
    form = SignUpForm()
    if request.method == 'POST':
        data = request.POST.dict()
        user = User.objects.create_user(
            data['username'], data['email'], data['password'])
        user.save()
        new_user = User.objects.get(username=data['username'])
        developer = Group.objects.get(name='Developer')
        developer.user_set.add(new_user)
        return HttpResponseRedirect('/karenzibohscrumy/successpage')
    else:
        form
    return render(request, 'registration/signup.html', {'form': form})


def login(request):
    return render(request, 'registration/login.html')


def move_view(request, goal_id):
    try:
        goal = ScrumyGoals.objects.get(goal_id=goal_id)

    except ScrumyGoals.DoesNotExist:
        context = {'error': 'A record with that goal id does not exist'}
        return render(request, 'karenzibohscrumy/exception.html', context)
    else:
        current_user = request.user
        group = current_user.groups.all()[0].name
        user = {
            'current_user': current_user,
            'group': group
        }
    if request.method == 'POST':
        form = MoveGoalForm(request.POST, instance=goal)
        goal = form.save(commit=False)
        data = request.POST.dict()
    #   print(data['goal_status'], group == 'Developer')
        goal_status = GoalStatus.objects.get(id=data['goal_status'])
        goal_status = str(goal_status)
        user = goal.user
        if group == 'Developer' and (goal_status == 'Done Goal' or current_user != user):
            ctx = {'error': 'Developers can only edit their own goals'}
            return render(request, 'karenzibohscrumy/exception.html', ctx)

        elif group == 'Quality Assurance':
            ctx = {}
            if current_user == user and goal_status == 'Weekly Goal':
                ctx['error'] = 'QA members can only move goals between daily, verify and done columns.'
                return render(request, 'karenzibohscrumy/exception.html', ctx)

            elif current_user != user and goal_status != 'Done Goal':
                ctx['error'] = 'QA members can only move other people\'s goals to the done column.'
                return render(request, 'karenzibohscrumy/exception.html', ctx)

        elif group == 'Owner' and user != current_user:
            ctx = {'error': 'Members of the Owner group can only edit their own goals'}
            return render(request, 'karenzibohscrumy/exception.html', ctx)

        goal.save()
        return HttpResponseRedirect('/karenzibohscrumy/home')

    form = MoveGoalForm(instance=goal)
    return render(request, 'karenzibohscrumy/movegoal.html', {'form': form})


@login_required
def add_goal(request):
    current_user = request.user
    group = current_user.groups.all()[0].name
    print(group)
    user = {
        'current_user': current_user,
        'group': group
    }
    if request.method == 'POST':
        form = CreateGoalForm(request.POST)
        goal = form.save(commit=False)
        data = request.POST.dict()
        print(data['goal_status'])
        goal_status = GoalStatus.objects.get(id=data['goal_status'])
        goal_status = str(goal_status)
        print(group in ['Developer', 'Quality Assurance'])
        # print(data['goal_status'] == '7')
        goal.moved_by = goal.user
        goal.created_by = goal.user
        goal.owner = goal.user
        username = goal.user
        if (group in ['Developer', 'Quality Assurance']) and (goal_status != 'Weekly Goal' or username != current_user):
            ctx = {
                'error': f'Members of the {group} group can only create weekly goals for themselves'}
            return render(request, 'karenzibohscrumy/exception.html', ctx)

        elif group == 'Owner' and goal_status != 'Weekly Goal':
            ctx = {'error': 'Members of the Owner group can only create weekly goals'}
            return render(request, 'karenzibohscrumy/exception.html', ctx)

        random_list = list(random.sample(range(1000, 9999), 10))
        random.shuffle(random_list)
        random_no = random_list[0]
        goal.goal_id = random_no
        goal.save()
        return HttpResponseRedirect('/karenzibohscrumy/home')

    form = CreateGoalForm()
    return render(request, 'karenzibohscrumy/addgoal.html', {'form': form})
