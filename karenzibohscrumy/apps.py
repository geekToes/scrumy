from django.apps import AppConfig


class KarenzibohscrumyConfig(AppConfig):
    name = 'karenzibohscrumy'
