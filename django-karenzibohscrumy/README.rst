============================
       KARENZIBOHSCRUMY
============================
Karenzibohscrumy is a django-based application
to test my knowledge in python and the Django framework.
All my linuxjobber training assignments are contained here.

1. Add "karenzibohscrumy" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'karenzibohscrumy',
    ]

2. Include the karenzibohscrummy URLconf in your project urls.py like this::

    path('karenzibohscrummy/', include('karenzibohscrummy.urls')),

3. Run `python manage.py migrate` to create the karenzibohscrumy models.

4. Start the development server using the command 'python manage.py runserver'

5. Visit http://127.0.0.1:8000/karenzibohscrumys/ to check out how my assignments came through.