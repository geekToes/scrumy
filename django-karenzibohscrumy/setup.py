import setuptools

with open("README.rst", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="karenziboh",  # Replace with your own username
    version="0.0.1",
    author="Karen Ziboh",
    author_email="karenziboh@gmail.com.com",
    description="Linuxjobber training labs",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/geekToes/scrumy.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
